## Instalation

1. Clone the repository and move inside the project folder
```sh
$ git clone git@github.com:4GeeksAcademy/django-rest-hello.git
$ cd tictactoe-api-django
```

2. Make sure you have python 3
```
$ python --version
```
Note: to migrate from python 2 to version 3 in cloud9 you need to do the following:
```sh
$ sudo mv /usr/bin/python /usr/bin/python2 
$ sudo ln -s /usr/bin/python3 /usr/bin/python
```

3. Run the pip install based on the requierments.txt
```sh
$ sudo pip install -r requirements.txt
```

4. Run migrations
```
$ python manage.py migrate
```

5. Add your domain in allowed domains
```
```

5. Run the server with
```
$ python manage.py runserver $IP:$PORT
```